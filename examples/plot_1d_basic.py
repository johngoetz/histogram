"""
About as basic an example as one can get.
"""
import numpy as np
from histogram import Histogram, plothist

np.random.seed(1)

h = Histogram(30, [0, 10])
h.fill(np.random.normal(5, 1, 1000))
print(h)
plothist(h, alpha=0.3)
